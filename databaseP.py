"""
UVG
Algoritmos y Estructuras de Datos
Rodrigo Zea - 17058
Francisco Molina - 17050
Base de Datos - Proyecto 2 
"""

from neo4jrestclient.client import GraphDatabase
from neo4jrestclient import client

db = GraphDatabase("http://localhost:7474", username="MySoulmate", password="mypassword")
cualidades = db.labels.create("Cualidades")
personas = db.labels.create("Personas")

# Cualidades y/o gustos de las personas
alto = db.nodes.create(name="Alto")
bajo = db.nodes.create(name="Bajo")
delgado = db.nodes.create(name="Delgado")
edad = db.nodes.create(name="Edad")
carismatico = db.nodes.create(name="Carismatico")
gracioso = db.nodes.create(name="Gracioso")
serio = db.nodes.create(name="Serio")
honesto=db.nodes.create(name="Honesto")
timido=db.nodes.create(name="Timido")
estudioso=db.nodes.create(name="intelectual")
ateo=db.nodes.create(name="ateo")
guapo=db.nodes.create(name="guapo")
# agregar cualidades
cualidades.add(alto, bajo, edad, carismatico, gracioso, serio, honesto,timido,estudioso,ateo,guapo)


# Haciendo personas...
esturban = db.nodes.create(name="Luis Esturban")
zea = db.nodes.create(name="Rodrigo Zea")
fm= db.nodes.create(name= "Francisco Molina")
marian = db.nodes.create(name="Maria Andree Estrada")
majo=db.nodes.create(name="Maria Jose Estrada")
odalis=db.nodes.create(name="Odalis Reyes")
mariana=db.nodes.create(name="Mariana Gall")
deus=db.nodes.create(name="Sebastian Arriola")
#add personas
personas.add(esturban,zea,fm,marian, majo,odalis,mariana,deus)

#es..
fm.relationships.create("es",carismatico)
fm.relationships.create("es",gracioso)
fm.relationships.create("es",alto)
fm.relationships.create("es",ateo)

marian.relationships.create("es",bajo)
marian.relationships.create("es",estudioso)
marian.relationships.create("es",honesto)
marian.relationships.create("es",timido)

zea.relationships.create("es",bajo)
zea.relationships.create("es",estudioso)
zea.relationships.create("es",delgado)
zea.relationships.create("es",serio)

majo.relationships.create("es",honesto)
majo.relationships.create("es",estudioso)

esturban.relationships.create("es",gracioso)
esturban.relationships.create("es",alto)
esturban.relationships.create("es",carismatico)

odalis.relationships.create("es",bajo)
odalis.relationships.create("es",honesto)
odalis.relationships.create("es",estudioso)
odalis.relationships.create("es",guapo)

mariana.relationships.create("es",alto)
mariana.relationships.create("es",guapo)
mariana.relationships.create("es",carismatico)

deus.relationships.create("es",alto)
deus.relationships.create("es",estudioso)
deus.relationships.create("es",carismatico)
deus.relationships.create("es",serio)
deus.relationships.create("es",timido)

#gustos
fm.relationships.create("le gusta", alto)
fm.relationships.create("le gusta", delgado)
fm.relationships.create("le gusta", ateo)



